const Sequelize=require('sequelize');

module.exports=(sequelize,DataTypes)=>{

    const ispit=sequelize.define("ispit",{
        nazivPredmeta:Sequelize.STRING,
        studijskaGodina:Sequelize.INTEGER,
        datumIVrijemeIspita:Sequelize.DATE
    },
    {
        freezeTableName:true,
        timestamps:false
    });
    return ispit;
}