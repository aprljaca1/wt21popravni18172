const Sequelize=require('sequelize');

module.exports=(sequelize,DataTypes)=>{
    const rezultat=sequelize.define("rezultat",{
        brojBodova:Sequelize.INTEGER,
        datumIVrijemeUnosa:Sequelize.DATE
    },
    {
        freezeTableName:true,
        timestamps:false
    })
    return rezultat;
}