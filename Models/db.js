const Sequelize=require('sequelize');
const sequelize=new Sequelize("popravni","root","password",{host:"localhost",dialect:"mysql",logging:false});
const db={};

db.Sequelize=Sequelize;
db.sequelize=sequelize;


db.student=require('./Student.js')(sequelize,Sequelize);
db.ispit=require('./Ispit.js')(sequelize,Sequelize);
db.rezultat=require('./Rezultat.js')(sequelize,Sequelize);

db.student.hasMany(db.rezultat);
db.rezultat.belongsTo(db.student);

db.ispit.hasMany(db.rezultat);
db.rezultat.belongsTo(db.ispit);



module.exports=db;