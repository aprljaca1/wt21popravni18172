const Sequelize=require('sequelize');

module.exports=(sequelize,DataTypes)=>{
    const student=sequelize.define("student",{
        ime:Sequelize.STRING,
        prezime:Sequelize.STRING,
        index:Sequelize.STRING,
        studijskaGodina:Sequelize.INTEGER
    },
    {
        freezeTableName:true,
        timestamps:false
    })
    return student;
}