const express=require('express');
const Sequelize=require('sequelize');
const app=express();
const env=process.env.NODE_ENV||3000;
const db=require('./Models/db.js');


app.use(express.json());
app.use(express.urlencoded({extended:false}));

const student=db.student;
const ispit=db.ispit;
const rezultat=db.rezultat;

app.post('/student',async(req,res)=>{

    if(req.body.studijskaGodina<1 || req.body.studijskaGodina>5)
    {
        res.status(400).json({errors:"Neisprvan studijska godina."});

    }
    else
    {
        var s={
            ime:req.body.ime,
            prezime:req.body.prezime,
            index:req.body.index,
            studijskaGodina:req.body.studijskaGodina
        };

        var c=await student.create(s);
        res.status(200).json(s);

    }


});


app.post('/ispit',async(req,res)=>{

    var parts=req.body.datumIVrijemeIspita.split('-');
    var datum=new Date(parts[0],parts[1]-1,parts[2]);
    var f=datum.toDateString();
    console.log(f);
    //return;
    var  i={
        nazivPredmeta:req.body.nazivPredmeta,
        studijskaGodina:req.body.studijskaGodina,
        datumIVrijemeIspita:f
    };
    var c=await ispit.create(i);
    res.status(200).json(i);

});



app.post('/rezultat',async(req,res)=>{

    var errorList=[];
    if(req.body.brojBodova<0 || req.body.brojBodova>100)
    {
       errorList.push("Broj bodova je neispravan");
    }
    var ispitIzBaze = await ispit.findOne({where:{id:req.body.ispitId}});
    var studentIzBaze = await student.findOne({where:{id:req.body.studentId}});

    if(ispitIzBaze.studijskaGodina!=studentIzBaze.studijskaGodina)
    {
        errorList.push("Student nije upisan na ispit");
    }

    if(ispitIzBaze==null || studentIzBaze==null)
    {
        errorList.push("Student/Ispit/Rezultat ne postoje");
    }

   
    var parts=req.body.datumIVrijemeIspita.split('-');
    var datum=new Date(parts[0],parts[1]-1,parts[2]);
    var f=datum.toDateString();
    var razlika=Math.abs(ispitIzBaze.datumIVrijemeIspita-f);
    var dani=razlika/(1000*3600*24);
    if(f<ispitIzBaze.datumIVrijemeIspita || razlika>=5 )
    {
        errorList.push("Pogrešan datum i vrijeme unosa rezultata");
    }

    if(errorList.length>0)
    {
        res.status(400).json({errors:JSON.stringify(errorList)});
        return;
    }
    var r={
        brojBodova:req.body.brojBodova,
        datumIVrijemeUnosa:f,
        studentId:req.body.studentId,
        ispitId:req.body.ispitId
    };

    var c=await rezultat.create(r);
    res.status(200).json(r);

});

app.put('/rezultat/:id',async(req,res)=>{

    var errorList=[];
    if(req.body.brojBodova<0 || req.body.brojBodova>100)
    {
       errorList.push("Broj bodova je neispravan");
    }
    if(errorList.length>0)
    {
        res.status(400).json({errors:JSON.stringify(errorList)});
        return;
    }

    var r=await rezultat.findOne({where:{id:req.params.id}});
    r.set({brojBodova:req.body.brojBodova,datumIVrijemeUnosa:req.body.datumIVrijemeUnosa});
    await r.save();

    res.status(204).json({status:"Uspjesno azurirano."});

})


db.sequelize.sync({force:false}).then((req)=>{
    app.listen(env,()=>{
        console.log("Server konektovan na port: "+env);
    });
});